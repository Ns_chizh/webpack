const path = require("path")
module.exports = {
    entry: [
        'webpack-dev-server/client/?http://localhost:8080',
        './src/components/app.jsx'
    ],
    resolve: {
        extensions: ['.js', '.jsx']
    },
    devtool: 'source-map',
    module: {
        rules:  [
            { test: /\.css$/, loader: 'style-loader!css-loader' },
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loader: ['react-hot-loader/webpack', 'babel-loader']
            }
        ]
    },
}