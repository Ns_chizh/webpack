import * as React from 'react'
import ReactDOM from 'react-dom'
import {Content}  from './Content'
import '../css/main.css'

ReactDOM.render(
    <Content />,
    document.getElementById('content')
)