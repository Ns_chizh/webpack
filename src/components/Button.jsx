import {jsx, css} from '@emotion/core'

const style = css`
  color: hotpink;
`

const SomeComponent = ({children}) => (
    <div css={style}>
        Some hotpink text.
        {children}
    </div>
)

const anotherStyle = css({
    textDecoration: 'underline'
})

export const AnotherComponent = () => (
    <div css={anotherStyle}>Some text with an underline.</div>
)
render(
    <SomeComponent>
        <AnotherComponent/>
    </SomeComponent>
)