import * as React from 'react'
import back_1 from '../img/back_1.jpg'
import back_2 from '../img/back_2.jpg'
import back_3 from '../img/back_3.png'
import back_4 from '../img/back_4.jpg'
import back_5 from '../img/back_5.jpg'
/** @jsx jsx */
import {css} from '@emotion/core'

export const Content = () => {
    const button = css`
    width: 140px;
    height: 100px;
    margin: 0 auto;
    padding: 200px 0;`

    const backgrounds = [
        back_1, back_2, back_3, back_4, back_5
    ]
    const [index, setIndex] = React.useState(0)

    const changeColor = (event) => {
        event.preventDefault()
        setIndex(prevIndex => prevIndex > (backGrounds.length - 2) ? 0 : prevIndex + 1)
    }

    return (
        <div className="content" style={{backgroundImage: `url(${backgrounds[index]})`}}>
            <div className="button" css={button}>
                <a href="#" onClick={changeColor}>Click</a>
            </div>
        </div>
    )

}
